import { Fragment, useState } from 'react';

declare global {
  interface Window {
    // this property will be globally injected by react native
    // https://github.com/react-native-community/react-native-webview/blob/master/docs/Guide.md#the-windowreactnativewebviewpostmessage-method-and-onmessage-prop
    ReactNativeWebView?: {
      postMessage: (message: string) => void;
    };
  }
}

export default function Home() {
  const [userID, setUserID] = useState('');
  const [token, setToken] = useState('');

  function handleSignUp() {
    const urlParams = new URLSearchParams(window.location.search);
    const identifier = urlParams.get('identifier');
    const payload = { token, identifier, userID };
    const serializedPayload = JSON.stringify(payload);
    window.ReactNativeWebView?.postMessage(serializedPayload);
  }

  return (
    <Fragment>
      <h1>BAM Sign Up</h1>
      <input
        placeholder="User ID"
        value={userID}
        onChange={({ currentTarget }) => setUserID(currentTarget.value)}
      />
      <br />
      <input
        placeholder="Token"
        value={token}
        onChange={({ currentTarget }) => setToken(currentTarget.value)}
      />
      <br />
      <button onClick={handleSignUp}>Sign Up</button>
    </Fragment>
  );
}
